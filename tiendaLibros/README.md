# README

Es una página web de ejemplo de tipo tienda, en este caso de libros.

# COMO EJECUTAR LA WEB EN LOCAL
Primero ejecutamos el servidor, el elegido ha sido webrick: rails server webrick.
Para acceder a la página accedemos a: localhost:3000.
Ya esta configurado para que acceda directamente a la página principal de la tienda.
